package com.babestudios.covid19.corona.ui.countries.list

import androidx.viewbinding.ViewBinding
import com.babestudios.covid19.corona.R
import com.babestudios.base.list.BaseViewHolder
import com.babestudios.covid19.data.model.Country
import java.lang.IllegalStateException

class CountriesTypeFactory : CountriesAdapter.CountriesTypeFactory {
	override fun type(country: Country): Int =  R.layout.item_countries

	override fun holder(type: Int, binding:ViewBinding): BaseViewHolder<*> {
		return when(type) {
			R.layout.item_countries -> CountriesViewHolder(binding)
			else -> throw IllegalStateException("Illegal view type")
		}
	}
}
