package com.babestudios.covid19.corona.ui.model

import com.babestudios.covid19.data.model.CovidHistory
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*

const val DATA_SET_LEGEND_TEXT_SIZE = 9f

/**
 * Converted chart data.
 * The ui converts the 'normal' model classes to this, because this has Android classes in it.
 * Is this the right approach?
 */
data class CovidChartData(
	val casesData: CombinedData = CombinedData(),
	val deathsData: CombinedData = CombinedData(),
	val recoveredData: CombinedData = CombinedData()
)

fun CovidHistory.convertToChartData(color: Int): CovidChartData {
	val casesData = convertToCombinedData(
		this.casesCumulativeHistory,
		this.casesDailyHistory,
		color
	)
	val deathsData = convertToCombinedData(
		this.deathsCumulativeHistory,
		this.deathsDailyHistory,
		color
	)
	val recoveredData = convertToCombinedData(
		this.recoveredCumulativeHistory,
		this.recoveredDailyHistory,
		color
	)
	return CovidChartData(casesData, deathsData, recoveredData)
}

fun convertToCombinedData(
	cumulative: Map<String, Int>,
	daily: Map<String, Int>,
	color: Int
): CombinedData {
	val lineDataSet = LineDataSet(cumulative.keys.mapIndexed { index, key ->
		Entry(index.toFloat(), cumulative[key]?.toFloat() ?: 0f)
	}, "")
	lineDataSet.valueTextSize = DATA_SET_LEGEND_TEXT_SIZE

	val barDataSet = BarDataSet(daily.keys.mapIndexed { index, key ->
		BarEntry(index.toFloat(), daily[key]?.toFloat() ?: 0f)
	}, "")
	barDataSet.valueTextSize = DATA_SET_LEGEND_TEXT_SIZE
	barDataSet.axisDependency = YAxis.AxisDependency.RIGHT
	barDataSet.color = color
	return CombinedData().apply {
		this.setData(LineData(lineDataSet))
		this.setData(BarData(barDataSet))
	}
}
