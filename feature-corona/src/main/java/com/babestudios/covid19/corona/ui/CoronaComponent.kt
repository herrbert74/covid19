package com.babestudios.covid19.corona.ui

import com.babestudios.base.di.scope.ActivityScope
import com.babestudios.covid19.core.injection.CoreComponent
import com.babestudios.covid19.data.repositories.CovidRepositoryContract
import com.babestudios.covid19.navigation.features.CoronaNavigator
import dagger.Component

@ActivityScope
@Component(dependencies = [CoreComponent::class])
interface CoronaComponent {
	fun navigator(): CoronaNavigator
	fun novelCovidRepository(): CovidRepositoryContract
}
