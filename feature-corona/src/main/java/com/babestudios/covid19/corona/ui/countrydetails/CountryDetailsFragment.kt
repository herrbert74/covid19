package com.babestudios.covid19.corona.ui.countrydetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.airbnb.mvrx.*
import com.babestudios.base.mvrx.BaseFragment
import com.babestudios.covid19.corona.R
import com.babestudios.covid19.corona.databinding.FragmentCountryDetailsBinding
import com.babestudios.covid19.corona.ui.CoronaActivity
import com.babestudios.covid19.corona.ui.CoronaViewModel
import com.babestudios.covid19.corona.ui.model.CovidChartData
import com.babestudios.covid19.corona.ui.model.convertToChartData
import com.google.android.material.tabs.TabLayout
import com.kennyc.view.MultiStateView.ViewState.*
import io.reactivex.disposables.CompositeDisposable

class CountryDetailsFragment : BaseFragment() {

	private var covidChartData: CovidChartData? = null
	private val viewModel by existingViewModel(CoronaViewModel::class)
	private val callback: OnBackPressedCallback = (object : OnBackPressedCallback(true) {
		override fun handleOnBackPressed() {
			viewModel.coronaNavigator.popBackStack()
		}
	})

	private val eventDisposables: CompositeDisposable = CompositeDisposable()

	private var _binding: FragmentCountryDetailsBinding? = null
	private val binding get() = _binding!!

	//region life cycle

	override fun onCreateView(
		inflater: LayoutInflater, container: ViewGroup?,
		savedInstanceState: Bundle?
	): View {
		requireActivity().onBackPressedDispatcher.addCallback(this, callback)
		_binding = FragmentCountryDetailsBinding.inflate(inflater, container, false)
		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		initializeUI()
	}

	private fun initializeUI() {
		viewModel.logScreenView(this::class.simpleName.orEmpty())
		(activity as AppCompatActivity).setSupportActionBar(binding.tbCountryDetails)
		val toolBar = (activity as AppCompatActivity).supportActionBar

		toolBar?.setDisplayHomeAsUpEnabled(true)
		withState(viewModel) {
			toolBar?.title = it.selectedCountry?.country
		}
		binding.tbCountryDetails.setNavigationOnClickListener { viewModel.coronaNavigator.popBackStack() }
		withState(viewModel) { state ->
			binding.lblCountryDetailsTotalCases.text =
				state.selectedCountry?.cases.toString()
			binding.lblCountryDetailsTotalDeaths.text =
				state.selectedCountry?.deaths.toString()
			binding.lblCountryDetailsTotalRecovered.text =
				state.selectedCountry?.recovered.toString()
		}
		viewModel.fetchCountryHistory()

		binding.tlCountryDetails.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
			override fun onTabReselected(tab: TabLayout.Tab?) = Unit

			override fun onTabUnselected(tab: TabLayout.Tab?) = Unit

			override fun onTabSelected(tab: TabLayout.Tab?) {
				tab?.let {
					showChartTab(tab.position)
				}
			}

		})
	}

	override fun onResume() {
		super.onResume()
		observeActions()
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
		callback.remove()
	}

	override fun orientationChanged() {
		val activity = requireActivity() as CoronaActivity
		viewModel.setNavigator(activity.injectCoronaNavigator())
	}

	//endregion

	//region render
	//endregion

	//region events

	private fun observeActions() {
		eventDisposables.clear()
		/*binding.btnCountryDetails.apply {
			RxView.clicks(this)
				.subscribe {
					viewModel.btnClicked()
				}
				?.let { btnDisposable -> eventDisposables.add(btnDisposable) }
		}*/
	}

	override fun invalidate() {
		withState(viewModel) { state ->
			when (state.countryDetailsRequest) {
				is Loading -> binding.msvCountryDetails.viewState = LOADING
				is Fail -> {
					binding.msvCountryDetails.viewState = ERROR
					val tvMsvError = binding.msvCountryDetails
						.findViewById<TextView>(R.id.tvMsvError)
					tvMsvError.text = state.countryDetailsRequest.error.message
				}
				is Success -> {
					binding.msvCountryDetails.viewState = CONTENT
					binding.chartCountryDetails.legend.isEnabled = false
					covidChartData = state
						.countryHistory
						?.covidDailyData
						?.convertToChartData(
							ContextCompat.getColor(
								requireContext(),
								R.color.colorAccent
							)
						)
					showChartTab(0)
				}
				else -> {
				}
			}
		}
	}

	private fun showChartTab(tab: Int) {
		binding.chartCountryDetails.clear()
		withState(viewModel) {
			covidChartData?.let {
				when (tab) {
					0 -> {
						binding.chartCountryDetails.data = it.casesData
					}
					1 -> {
						binding.chartCountryDetails.data = it.deathsData
					}
					2 -> {
						binding.chartCountryDetails.data = it.recoveredData
					}
				}

				binding.chartCountryDetails.invalidate()
			}
		}
	}
	//endregion
}
