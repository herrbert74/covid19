package com.babestudios.covid19.corona.ui.countries.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.babestudios.base.list.BaseViewHolder
import com.babestudios.covid19.corona.databinding.ItemCountriesBinding
import com.babestudios.covid19.data.model.Country
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class CountriesAdapter(private var countriesVisitables: List<AbstractCountriesVisitable>,
							private val countriesTypeFactory: CountriesTypeFactory)
	: RecyclerView.Adapter<BaseViewHolder<AbstractCountriesVisitable>>() {

	override fun getItemCount(): Int {
		return countriesVisitables.size
	}

	override fun getItemViewType(position: Int): Int {
		return countriesVisitables[position].type(countriesTypeFactory)
	}

	private val itemClickSubject = PublishSubject.create<BaseViewHolder<AbstractCountriesVisitable>>()

	fun getViewClickedObservable(): Observable<BaseViewHolder<AbstractCountriesVisitable>> {
		return itemClickSubject
	}

	interface CountriesTypeFactory {
		fun type(country: Country): Int
		fun holder(type: Int, binding: ViewBinding): BaseViewHolder<*>
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<AbstractCountriesVisitable> {
		val binding = ItemCountriesBinding.inflate(
				LayoutInflater.from(parent.context),
				parent,
				false)
		val v = countriesTypeFactory.holder(viewType, binding) as BaseViewHolder<AbstractCountriesVisitable>
		RxView.clicks(binding.root)
				.takeUntil(RxView.detaches(parent))
				.map { v }
				.subscribe(itemClickSubject)
		return v
	}

	override fun onBindViewHolder(holder: BaseViewHolder<AbstractCountriesVisitable>, position: Int) {
		holder.bind(countriesVisitables[position])
	}

	fun updateItems(visitables: List<AbstractCountriesVisitable>) {
		countriesVisitables = visitables
		notifyDataSetChanged()
	}
}
