package com.babestudios.covid19.corona.ui

import com.airbnb.mvrx.Async
import com.airbnb.mvrx.MvRxState
import com.airbnb.mvrx.PersistState
import com.airbnb.mvrx.Uninitialized
import com.babestudios.covid19.corona.ui.countries.list.AbstractCountriesVisitable
import com.babestudios.covid19.data.model.Country
import com.babestudios.covid19.data.model.CountryCovidHistory
import com.babestudios.covid19.data.model.CovidHistory

data class CoronaState(
	val countriesRequest: Async<List<Country>> = Uninitialized,
	val countries: List<AbstractCountriesVisitable> = emptyList(),
	val totalCountriesCount: Int = 0,
	@PersistState val selectedCountry: Country? = null,

	//Country details
	val countryDetailsRequest: Async<CountryCovidHistory> = Uninitialized,
	val countryHistory: CountryCovidHistory? = null,
	@PersistState val iso3: String = "",

	//global
	val globalResultsRequest: Async<CovidHistory> = Uninitialized,
	val globalHistory: CovidHistory? = null
) : MvRxState
