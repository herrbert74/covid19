package com.babestudios.covid19.corona.ui

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.babestudios.base.ext.isLazyInitialized
import com.babestudios.base.mvrx.BaseActivity
import com.babestudios.covid19.corona.R
import com.babestudios.covid19.core.injection.CoreInjectHelper
import com.babestudios.covid19.data.repositories.CovidRepositoryContract
import com.babestudios.covid19.navigation.features.CoronaNavigator

class CoronaActivity : BaseActivity() {

	private val comp by lazy {
		DaggerCoronaComponent
				.builder()
				.coreComponent(CoreInjectHelper.provideCoreComponent(applicationContext))
				.build()
	}

	private lateinit var coronaNavigator: CoronaNavigator

	private lateinit var navController: NavController

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_corona)
		navController = findNavController(R.id.navHostFragmentCorona)
		if (::comp.isLazyInitialized) {
			coronaNavigator.bind(navController)
		}
	}

	override fun onBackPressed() {
		if (onBackPressedDispatcher.hasEnabledCallbacks()) {
			onBackPressedDispatcher.onBackPressed()
		} else {
			super.finish()
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out)
		}
	}
	fun injectNovelCovidRepository(): CovidRepositoryContract {
		return comp.novelCovidRepository()
	}

	fun injectCoronaNavigator(): CoronaNavigator {
		coronaNavigator = comp.navigator()
		if (::navController.isInitialized)
			coronaNavigator.bind(navController)
		return coronaNavigator
	}
}
