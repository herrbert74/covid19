package com.babestudios.covid19.corona.ui.countries.list

import androidx.viewbinding.ViewBinding
import com.babestudios.base.list.BaseViewHolder
import com.babestudios.covid19.corona.R
import com.babestudios.covid19.corona.databinding.ItemCountriesBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class CountriesViewHolder(_binding: ViewBinding) : BaseViewHolder<AbstractCountriesVisitable>(_binding) {
	override fun bind(visitable: AbstractCountriesVisitable) {
		val binding = _binding as ItemCountriesBinding
		val country = (visitable as CountriesVisitable).country
		binding.lblCountryName.text = country.country
		binding.lblCountryCasesCount.text = String.format(binding.root.context.getString(
			R.string.countries_cases), country.cases.toString())
		binding.lblCountryDeathCount.text = String.format(binding.root.context.getString(
			R.string.countries_deaths), country.deaths.toString())
		binding.lblCountryNewCasesCount.text = "+${country.todayCases}"
		binding.lblCountryNewDeathCount.text = "+${country.todayDeaths}"
		Glide.with(itemView.context)
			.load(country.countryInfo.flag)
			.apply(RequestOptions.circleCropTransform())
			.into(binding.ivCountryFlag)
	}
}

