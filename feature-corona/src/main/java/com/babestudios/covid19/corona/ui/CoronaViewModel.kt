package com.babestudios.covid19.corona.ui

import com.airbnb.mvrx.MvRxViewModelFactory
import com.airbnb.mvrx.ViewModelContext
import com.babestudios.base.mvrx.BaseViewModel
import com.babestudios.covid19.corona.ui.countries.list.AbstractCountriesVisitable
import com.babestudios.covid19.corona.ui.countries.list.CountriesVisitable
import com.babestudios.covid19.data.repositories.CovidRepositoryContract
import com.babestudios.covid19.navigation.features.CoronaNavigator

import com.babestudios.covid19.data.model.Country

class CoronaViewModel(
	coronaState: CoronaState,
	private val covidRepository: CovidRepositoryContract,
	var coronaNavigator: CoronaNavigator
) : BaseViewModel<CoronaState>(coronaState, covidRepository) {

	companion object : MvRxViewModelFactory<CoronaViewModel, CoronaState> {

		@JvmStatic
		override fun create(
			viewModelContext: ViewModelContext,
			state: CoronaState
		): CoronaViewModel? {
			val novelCovidRepository =
				viewModelContext.activity<CoronaActivity>().injectNovelCovidRepository()
			val coronaNavigator =
				viewModelContext.activity<CoronaActivity>().injectCoronaNavigator()
			return CoronaViewModel(
				state,
				novelCovidRepository,
				coronaNavigator
			)
		}

	}
	//region countries

	fun fetchCountries() {
		covidRepository.fetchCountries()
			.execute {
				copy(
					countriesRequest = it,
					countries = convertToVisitables(it())
				)
			}
	}

	private fun convertToVisitables(reply: List<Country>?): List<AbstractCountriesVisitable> {
		return ArrayList(reply?.map { item -> CountriesVisitable(item) } ?: emptyList())
	}

	fun countryClicked(adapterPosition: Int) {
		withState { state ->
			setState {
				copy(
					selectedCountry = (state.countries[adapterPosition] as CountriesVisitable).country
				)
			}
		}
		coronaNavigator.countriesToCountryDetails()
	}

	//endregion

	//region country details

	fun fetchCountryHistory() {
		withState { state ->
			state.selectedCountry?.countryInfo?.iso3?.let { iso3 ->
				covidRepository.fetchCountryHistory(iso3)
					.execute {
						copy(
							countryDetailsRequest = it,
							countryHistory = it()
						)
					}
			}
		}
	}
	//endregion

	//region global results

	fun fetchGlobalHistory() {
		withState {
			covidRepository.fetchGlobalHistory()
				.execute {
					copy(
						globalResultsRequest = it,
						globalHistory = it()
					)
				}
		}
	}

	fun setNavigator(navigator: CoronaNavigator) {
		coronaNavigator = navigator
	}

	//endregion

}
