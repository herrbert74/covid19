package com.babestudios.covid19.corona.ui.countries.list

import com.babestudios.covid19.data.model.Country

@Suppress("UnnecessaryAbstractClass")
abstract class AbstractCountriesVisitable{
	abstract fun type(countriesTypeFactory: CountriesAdapter.CountriesTypeFactory): Int
}

class CountriesVisitable(val country: Country) : AbstractCountriesVisitable(){
	override fun type(countriesTypeFactory: CountriesAdapter.CountriesTypeFactory): Int {
		return countriesTypeFactory.type(country)
	}
}
