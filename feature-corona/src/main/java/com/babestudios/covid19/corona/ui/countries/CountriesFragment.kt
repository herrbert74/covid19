package com.babestudios.covid19.corona.ui.countries

import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.mvrx.BaseMvRxFragment
import com.airbnb.mvrx.*
import com.airbnb.mvrx.withState
import com.airbnb.mvrx.activityViewModel
import com.kennyc.view.MultiStateView.ViewState.*
import com.babestudios.base.list.BaseViewHolder
import com.babestudios.base.mvrx.BaseFragment
import com.babestudios.base.view.DividerItemDecoration
import com.babestudios.covid19.corona.R
import com.babestudios.covid19.corona.databinding.FragmentCountriesBinding
import com.babestudios.covid19.corona.ui.CoronaActivity
import com.babestudios.covid19.corona.ui.CoronaState
import com.babestudios.covid19.corona.ui.CoronaViewModel
import com.babestudios.covid19.corona.ui.countries.list.*
import io.reactivex.disposables.CompositeDisposable

class CountriesFragment : BaseFragment() {

	private val viewModel by activityViewModel(CoronaViewModel::class)
	private var countriesAdapter: CountriesAdapter? = null

	private val eventDisposables: CompositeDisposable = CompositeDisposable()

	private var _binding: FragmentCountriesBinding? = null
	private val binding get() = _binding!!

	//region life cycle

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setHasOptionsMenu(true)
	}

	override fun onCreateView(
			inflater: LayoutInflater, container: ViewGroup?,
			savedInstanceState: Bundle?
	): View {
		_binding = FragmentCountriesBinding.inflate(inflater, container, false)
		setHasOptionsMenu(true)
		return binding.root
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		initializeUI()
	}

	private fun initializeUI() {
		viewModel.logScreenView(this::class.simpleName.orEmpty())
		(activity as AppCompatActivity).setSupportActionBar(binding.tbCountries)
		val toolBar = (activity as AppCompatActivity).supportActionBar

		toolBar?.setDisplayHomeAsUpEnabled(true)
		binding.tbCountries.setNavigationOnClickListener { activity?.onBackPressed() }
		createRecyclerView()
		viewModel.fetchCountries()
	}

	override fun onResume() {
		super.onResume()
		observeActions()
	}

	override fun onDestroyView() {
		super.onDestroyView()
		_binding = null
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		inflater.inflate(R.menu.menu_countries, menu)
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		return when (item.itemId) {
			R.id.action_global -> {
				viewModel.coronaNavigator.countriesToGlobalResults()
				true
			}
			else -> super.onOptionsItemSelected(item)
		}
	}

	override fun orientationChanged() {
		val activity = requireActivity() as CoronaActivity
		viewModel.setNavigator(activity.injectCoronaNavigator())
	}

	//endregion

	//region render

	private fun createRecyclerView() {
		val linearLayoutManager = LinearLayoutManager(
				requireContext(),
				LinearLayoutManager.VERTICAL,
				false
		)
		binding.rvCountries.layoutManager = linearLayoutManager
		binding.rvCountries.addItemDecoration(DividerItemDecoration(requireContext()))
	}

	//endregion

	//region events

	private fun observeActions() {
		eventDisposables.clear()
		countriesAdapter?.getViewClickedObservable()
				?.take(1)
				?.subscribe { view: BaseViewHolder<AbstractCountriesVisitable> ->
					viewModel.countryClicked((view as CountriesViewHolder).adapterPosition)
				}
				?.let { eventDisposables.add(it) }
	}

	override fun invalidate() {
		withState(viewModel) { state ->
			when (state.countriesRequest) {
				is Loading -> binding.msvCountries.viewState = LOADING
				is Fail -> {
					binding.msvCountries.viewState = ERROR
					val tvMsvError = binding.msvCountries
							.findViewById<TextView>(R.id.tvMsvError)
					tvMsvError.text = state.countriesRequest.error.message
				}
				is Success -> {
					if (state.countries.isEmpty()) {
						binding.msvCountries.viewState = EMPTY
					} else {
						binding.msvCountries.viewState = CONTENT
						if (binding.rvCountries.adapter == null) {
							countriesAdapter = CountriesAdapter(state.countries, CountriesTypeFactory())
							binding.rvCountries.adapter = countriesAdapter
						} else {
							countriesAdapter?.updateItems(state.countries)
						}
					}
					observeActions()
				}
				else -> {
				}
			}
		}
	}

	//endregion
}
