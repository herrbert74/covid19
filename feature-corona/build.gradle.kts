import com.babestudios.covid19.buildsrc.Libs

plugins{
	id("com.babestudios.covid19.plugins.android")
	id("com.babestudios.covid19.plugins.feature")
}

dependencies {
	implementation(Libs.Views.mpAndrooidChart)
	implementation(Libs.glide)
}