import com.babestudios.covid19.buildsrc.Libs

plugins{
	id("com.babestudios.covid19.plugins.android")
}

dependencies {
	implementation(Libs.AndroidX.Navigation.ktx)
}
