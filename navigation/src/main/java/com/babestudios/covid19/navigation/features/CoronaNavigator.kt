package com.babestudios.covid19.navigation.features

import com.babestudios.covid19.navigation.base.Navigator

interface CoronaNavigator : Navigator {
	fun countriesToCountryDetails()
	fun countriesToGlobalResults()
}
