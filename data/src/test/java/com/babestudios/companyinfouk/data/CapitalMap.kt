package com.babestudios.companyinfouk.data

import com.babestudios.companyinfouk.data.model.filinghistory.Capital

class CapitalMap (
	var pages: Int,
	var capital: Map<String, Capital>
)