package com.babestudios.covid19.data.network

import com.babestudios.covid19.data.BuildConfig
import com.babestudios.covid19.data.dto.covidapiinfo.GlobalCountResult
import io.reactivex.Single
import retrofit2.http.GET

interface CovidApiInfoService {
	@GET(BuildConfig.COVID_API_INFO_GLOBAL_COUNT_ENDPOINT)
	fun fetchGlobalCountByDate(): Single<GlobalCountResult>
}
