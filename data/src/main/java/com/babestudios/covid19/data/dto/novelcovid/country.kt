package com.babestudios.covid19.data.dto.novelcovid

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CountryDto(
		val active: Int,
		val cases: Int,
		val casesPerOneMillion: Float,
		val continent: String,
		val country: String,
		val countryInfo: CountryInfoDto,
		val critical: Int,
		val deaths: Int,
		val deathsPerOneMillion: Float,
		val recovered: Int,
		val tests: Int,
		val testsPerOneMillion: Float,
		val todayCases: Int,
		val todayDeaths: Int,
		val updated: Long
) : Parcelable

@Parcelize
data class CountryInfoDto(
	val _id: Int,
	val flag: String,
	val iso2: String,
	val iso3: String,
	val lat: Float,
	val long: Float
) : Parcelable
