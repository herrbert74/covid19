package com.babestudios.covid19.data.dto.novelcovid

data class CountryHistoryDto(
	val country: String?,
	val provinces: List<String>,
	val timeline: TimelineDto
)

data class TimelineDto(
	val cases: Map<String, Int>,
	val deaths: Map<String, Int>,
	val recovered: Map<String, Int>
)
