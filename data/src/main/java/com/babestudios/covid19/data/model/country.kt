package com.babestudios.covid19.data.model

import android.os.Parcelable
import com.babestudios.covid19.data.dto.novelcovid.CountryInfoDto
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Country(
	val active: Int,
	val cases: Int,
	val casesPerOneMillion: Float,
	val country: String,
	val countryInfo: CountryInfo,
	val critical: Int,
	val deaths: Int,
	val deathsPerOneMillion: Float,
	val recovered: Int,
	val todayCases: Int,
	val todayDeaths: Int,
	val updated: Long
) : Parcelable

@Parcelize
data class CountryInfo(
	val _id: Int,
	val flag: String,
	val iso3: String?
) : Parcelable
