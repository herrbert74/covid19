package com.babestudios.covid19.data.utils.errors

import com.babestudios.base.rxjava.ErrorResolver
import com.babestudios.covid19.data.utils.errors.model.ErrorBody
import com.google.gson.Gson
import com.google.gson.JsonParseException
import io.reactivex.Single
import retrofit2.HttpException
import java.lang.IllegalStateException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Using the defaults here
 */

@Singleton
class NovelCovidErrorResolver @Inject constructor() : ErrorResolver {

	@Suppress("RemoveExplicitTypeArguments")
	override fun <T> resolveErrorForSingle(): (Single<T>) -> Single<T> {
		return { single ->
			single.onErrorResumeNext {
				(it as? HttpException)?.response()?.errorBody()?.let { body ->
					Single.error<T> { Exception(errorMessageFromResponseBody(body)) }
				} ?: Single.error<T> { Exception("An error happened") }
			}
		}
	}
}
