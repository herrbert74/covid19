package com.babestudios.covid19.data.dto.covidapiinfo

data class GlobalCountResult(
		val count: Int,
		val result: Map<String, Result>
)

data class Result(
		val confirmed: Int,
		val deaths: Int,
		val recovered: Int
)
