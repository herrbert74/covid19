package com.babestudios.covid19.data.model

/**
 * Formatted data that the ui consumes. Needs to be converted to chart data.
 * This is used on the country detail screen in a map, with country specific information.
 */
data class CountryCovidHistory(
	val country: String?,
	val provinces: List<String>,
	val covidDailyData: CovidHistory
)

/**
 * Formatted data that the ui consumes. Needs to be converted to chart data.
 * This is directly used on the global screen in a map.
 */
data class CovidHistory(
	val casesCumulativeHistory: Map<String, Int>,
	val casesDailyHistory: Map<String, Int>,
	val deathsCumulativeHistory: Map<String, Int>,
	val deathsDailyHistory: Map<String, Int>,
	val recoveredCumulativeHistory: Map<String, Int>,
	val recoveredDailyHistory: Map<String, Int>
)
