package com.babestudios.covid19.data.network

import com.babestudios.covid19.data.BuildConfig
import com.babestudios.covid19.data.dto.novelcovid.CountryDto
import com.babestudios.covid19.data.dto.novelcovid.CountryHistoryDto
import com.babestudios.covid19.data.dto.novelcovid.TimelineDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface NovelCovidService {
	@GET(BuildConfig.NOVEL_COVID_COUNTRIES_ENDPOINT)
	fun fetchCountries(): Single<List<CountryDto>>

	@GET(BuildConfig.NOVEL_COVID_HISTORICAL_COUNTRY_ENDPOINT)
	fun fetchCountryHistory(@Path("country") country: String): Single<CountryHistoryDto>

	/*@GET(BuildConfig.NOVEL_COVID_HISTORICAL_ALL_ENDPOINT)
	fun fetchAllHistory(): Single<TimelineDto>*/
}
