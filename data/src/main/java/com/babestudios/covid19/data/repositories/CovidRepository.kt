package com.babestudios.covid19.data.repositories

import android.os.Bundle
import com.babestudios.base.data.AnalyticsContract
import com.babestudios.base.rxjava.ErrorResolver
import com.babestudios.base.rxjava.SchedulerProvider
import com.babestudios.covid19.data.dto.novelcovid.CountryDto
import com.babestudios.covid19.data.model.*
import com.babestudios.covid19.data.network.CovidApiInfoService
import com.babestudios.covid19.data.network.NovelCovidService
import com.google.firebase.analytics.FirebaseAnalytics
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

interface CovidRepositoryContract : AnalyticsContract {
	//NovelCovid API
	fun fetchCountries(): Single<List<Country>>
	fun fetchCountryHistory(country: String): Single<CountryCovidHistory>

	//CovidApi.info API
	fun fetchGlobalHistory(): Single<CovidHistory>

	//Preferences
	//Empty for now
}

@Singleton
open class CovidRepository @Inject constructor(
	private val novelCovidService: NovelCovidService,
	private val covidApiInfoService: CovidApiInfoService,
	private val firebaseAnalytics: FirebaseAnalytics,
	private val schedulerProvider: SchedulerProvider,
	private val errorResolver: ErrorResolver
) : CovidRepositoryContract {

	//region NovelCovid API

	override fun fetchCountries(): Single<List<Country>> {
		return novelCovidService.fetchCountries()
			.compose(errorResolver.resolveErrorForSingle())
			.map {
				it.map { countryDto -> countryDto.toCountry() }
			}
			.compose(schedulerProvider.getSchedulersForSingle())
	}

	override fun fetchCountryHistory(country: String): Single<CountryCovidHistory> {
		return novelCovidService.fetchCountryHistory(country)
			.compose(errorResolver.resolveErrorForSingle())
			.map {
				val casesDaily = cumulativeToDaily(it.timeline.cases)
				val deathsDaily = cumulativeToDaily(it.timeline.deaths)
				val recoveredDaily = cumulativeToDaily(it.timeline.recovered)
				CountryCovidHistory(
					it.country,
					it.provinces,
					CovidHistory(
						it.timeline.cases,
						casesDaily,
						it.timeline.deaths,
						deathsDaily,
						it.timeline.recovered,
						recoveredDaily
					)
				)
			}
			.compose(schedulerProvider.getSchedulersForSingle())
	}

	/**
	 * Converts a cumulative map of dates and ints into a map of daily data
	 * Initial 0 added to keep data sizes consistent
	 */
	private fun cumulativeToDaily(map: Map<String, Int>): Map<String, Int> {
		return mapOf("" to 0).plus(map)
			.asSequence()
			.zipWithNext { a, b -> b.key to b.value - a.value }
			.toMap()
	}

	//endregion

	//region NovelCovid API

	override fun fetchGlobalHistory(): Single<CovidHistory> {
		return covidApiInfoService.fetchGlobalCountByDate()
			.compose(errorResolver.resolveErrorForSingle())
			.map {
				val k = mapOf(
					"" to com.babestudios.covid19.data.dto.covidapiinfo
						.Result(0, 0, 0)
				).plus(it.result)
				CovidHistory(
					it.result.mapValues { result -> result.value.confirmed },
					k.entries.asSequence().zipWithNext { a, b ->
						b.key to b.value.confirmed - a.value.confirmed
					}.toMap(),
					it.result.mapValues { result -> result.value.deaths },
					k.entries.asSequence().zipWithNext { a, b ->
						b.key to b.value.deaths - a.value.deaths
					}.toMap(),
					it.result.mapValues { result -> result.value.recovered },
					k.entries.asSequence().zipWithNext { a, b ->
						b.key to b.value.recovered - a.value.recovered
					}.toMap()
				)
			}
			.compose(schedulerProvider.getSchedulersForSingle())
	}

	//endregion

	//region analytics

	override fun logAppOpen() {
		firebaseAnalytics.logEvent(FirebaseAnalytics.Event.APP_OPEN, null)
	}

	override fun logScreenView(screenName: String) {
		val bundle = Bundle()
		bundle.putString("screen_name", screenName)
		firebaseAnalytics.logEvent("screenView", bundle)
	}

	override fun logSearch(queryText: String) {
		val bundle = Bundle()
		bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, queryText)
		firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SEARCH, bundle)
	}

	//endregion

}

private fun CountryDto.toCountry(): Country {
	return Country(
		this.active,
		this.cases,
		this.casesPerOneMillion,
		this.country,
		CountryInfo(this.countryInfo._id, this.countryInfo.flag, this.countryInfo.iso3),
		this.critical,
		this.deaths,
		this.deathsPerOneMillion,
		this.recovered,
		this.todayCases,
		this.todayDeaths,
		this.updated
	)
}
