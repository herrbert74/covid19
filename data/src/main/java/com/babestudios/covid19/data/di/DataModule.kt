package com.babestudios.covid19.data.di

import android.content.Context
import android.content.SharedPreferences
import android.os.AsyncTask
import com.babestudios.base.rxjava.SchedulerProvider
import com.babestudios.covid19.data.BuildConfig
import com.babestudios.covid19.data.local.PREF_FILE_NAME
import com.babestudios.covid19.data.network.CovidApiInfoService
import com.babestudios.covid19.data.network.NovelCovidService
import com.babestudios.covid19.data.network.converters.AdvancedGsonConverterFactory
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@Suppress("unused")
class DataModule(private val context: Context) {

	@Provides
	@Singleton
	@Named("NovelCovidRetrofit")
	internal fun provideNovelCovidRetrofit(): Retrofit {
		return getRetrofit(BuildConfig.NOVEL_COVID_BASE_URL)
	}

	@Provides
	@Singleton
	@Named("CovidApiInfoRetrofit")
	internal fun provideCovidApiInfoRetrofit(): Retrofit {
		return getRetrofit(BuildConfig.COVID_API_INFO_BASE_URL)
	}

	private fun getRetrofit(baseUrl: String): Retrofit {
		return Retrofit.Builder()//
				.baseUrl(baseUrl)//
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())//
				.addConverterFactory(AdvancedGsonConverterFactory.create())//
				.client(getOkHttpClientBuilder().build())//
				.build()
	}

	private fun getOkHttpClientBuilder(): OkHttpClient.Builder {
		val logging = HttpLoggingInterceptor()
		logging.level = HttpLoggingInterceptor.Level.BODY

		val httpClient = OkHttpClient.Builder()
		httpClient.addInterceptor(logging)
		httpClient.addInterceptor(ChuckerInterceptor(context))
		return httpClient
	}

	@Provides
	@Singleton
	internal fun provideNovelCovidService(@Named("NovelCovidRetrofit") retroFit: Retrofit)
			: NovelCovidService {
		return retroFit.create(NovelCovidService::class.java)
	}

	@Provides
	@Singleton
	internal fun provideCovidApiInfoService(@Named("CovidApiInfoRetrofit") retroFit: Retrofit)
			: CovidApiInfoService {
		return retroFit.create(CovidApiInfoService::class.java)
	}

	@Provides
	internal fun provideGson(): Gson {
		return GsonBuilder()
				.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz")
				.create()
	}

	@Provides
	internal fun provideSharedPreferences(): SharedPreferences {
		return context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE)
	}

	@Provides
	@Singleton
	internal fun provideSchedulerProvider(): SchedulerProvider {
		return SchedulerProvider(Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR), AndroidSchedulers.mainThread())
	}


	@Provides
	@Singleton
	internal fun provideFirebaseAnalytics(): FirebaseAnalytics {
		return FirebaseAnalytics.getInstance(context)
	}
}
