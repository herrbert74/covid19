package com.babestudios.covid19.data.di

import com.babestudios.base.rxjava.ErrorResolver
import com.babestudios.covid19.data.repositories.CovidRepository
import com.babestudios.covid19.data.repositories.CovidRepositoryContract
import com.babestudios.covid19.data.utils.errors.NovelCovidErrorResolver
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 * Using the [Binds] annotation to bind internal implementations to their interfaces results in less generated code.
 * We also get rid of actually using this module by turning it into an abstract class, then an interface,
 * hence it's separated from [DataModule]
 */
@Module
interface DataContractModule {

	@Singleton
	@Binds
	fun bindCovidRepositoryContract(covidRepository: CovidRepository): CovidRepositoryContract

	@Singleton
	@Binds
	fun provideErrorResolver(novelCovidErrorResolver: NovelCovidErrorResolver): ErrorResolver
}
