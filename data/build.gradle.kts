import com.babestudios.covid19.buildsrc.Libs

plugins {
	id("com.babestudios.covid19.plugins.android")
}

val companiesHouseApiKey: String by project

android {
	buildTypes {
		all {
			buildConfigField("String", "NOVEL_COVID_BASE_URL", "\"https://corona.lmao.ninja\"")

			buildConfigField("String", "NOVEL_COVID_COUNTRIES_ENDPOINT", "\"v2/countries\"")
			buildConfigField("String", "NOVEL_COVID_HISTORICAL_COUNTRY_ENDPOINT", "\"v2/historical/{country}\"")
			//buildConfigField("String", "NOVEL_COVID_HISTORICAL_ALL_ENDPOINT", "\"historical/all\"")

			buildConfigField("String", "COVID_API_INFO_BASE_URL", "\"https://covidapi.info\"")

			buildConfigField("String", "COVID_API_INFO_GLOBAL_COUNT_ENDPOINT", "\"api/v1/global/count\"")
		}
	}
}

dependencies {
	api(Libs.baBeStudiosBase)
	api(project(":common"))

	implementation(Libs.Kotlin.stdLibJdk8)

	implementation(Libs.AndroidX.appcompat)
	implementation(Libs.AndroidX.coreKtx)

	implementation(Libs.SquareUp.Retrofit2.retrofit)
	implementation(Libs.SquareUp.Retrofit2.rxJava2Adapter)
	implementation(Libs.SquareUp.Retrofit2.converterGson)

	api(Libs.SquareUp.OkHttp3.loggingInterceptor)

	implementation(Libs.RxJava2.rxJava)
	implementation(Libs.RxJava2.rxAndroid)
	implementation(Libs.RxJava2.rxKotlin)

	implementation(Libs.Google.Dagger.dagger)
	implementation(Libs.Google.Firebase.analytics)

	kapt(Libs.Google.Dagger.compiler)

	debugImplementation(Libs.debugDb)
	debugImplementation(Libs.Chucker.library)
	releaseImplementation(Libs.Chucker.noop)

	implementation(Libs.Javax.inject)
	implementation(Libs.Javax.annotations)

	testImplementation(Libs.AndroidX.Test.Ext.jUnit)
	testImplementation(Libs.Test.mockK)
}
