package com.babestudios.covid19

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.core.os.bundleOf
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import com.babestudios.covid19.navigation.base.BaseNavigator
import com.babestudios.covid19.navigation.di.NavigationComponent
import com.babestudios.covid19.navigation.features.*


/**
 * This class holds the navController for any feature through [BaseNavigator]
 * It is a dependency of [com.babestudios.covid19.core.injection.CoreComponent],
 * so no need to create it in every feature component.
 *
 * This holds all the implementations for [NavigationComponent],
 * and the feature navigations need to be exposed from the component,
 * like e.g. [com.babestudios.covid19.corona.ui.CoronaComponent.navigator].
 */
internal class Covid19Navigation : NavigationComponent {

	//region features

	override fun provideCoronaNavigation(): CoronaNavigator {
		return object : BaseNavigator(), CoronaNavigator {

			override fun countriesToCountryDetails() {
				navController?.navigateSafe(R.id.action_coronaFragment_to_coronaDetailsFragment)
			}

			override fun countriesToGlobalResults() {
				navController?.navigateSafe(R.id.action_coronaFragment_to_globalResultsFragment)
			}

			override var navController: NavController? = null

		}
	}
	//endregion
}

@Suppress("MaxLineLength")
		/**
		 * https://stackoverflow.com/questions/51060762/java-lang-illegalargumentexception-navigation-destination-xxx-is-unknown-to-thi
		 */
fun NavController.navigateSafe(
	@IdRes resId: Int,
	args: Bundle? = null,
	navOptions: NavOptions? = null,
	navExtras: Navigator.Extras? = null
) {
	val action = currentDestination?.getAction(resId) ?: graph.getAction(resId)
	if (action != null && currentDestination?.id != action.destinationId) {
		navigate(resId, args, navOptions, navExtras)
	}
}
