package com.babestudios.covid19.core.injection

import android.content.Context
import com.babestudios.base.di.qualifier.ApplicationContext
import com.babestudios.base.rxjava.ErrorResolver
import com.babestudios.base.rxjava.SchedulerProvider
import com.babestudios.covid19.data.repositories.CovidRepositoryContract
import com.babestudios.covid19.data.di.DataContractModule
import com.babestudios.covid19.data.di.DataModule
import com.babestudios.covid19.navigation.di.NavigationComponent
import com.babestudios.covid19.navigation.features.CoronaNavigator
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(
		modules = [DataModule::class, DataContractModule::class],
		dependencies = [NavigationComponent::class]
)
interface CoreComponent {

	@Component.Factory
	interface Factory {
		fun create(
				dataModule: DataModule,
				navigationComponent: NavigationComponent,
				@BindsInstance @ApplicationContext applicationContext: Context
		): CoreComponent
	}

	fun novelCovidRepository(): CovidRepositoryContract

	fun schedulerProvider(): SchedulerProvider

	fun errorResolver(): ErrorResolver

	@ApplicationContext
	fun context(): Context

	fun coronaNavigation(): CoronaNavigator
}
