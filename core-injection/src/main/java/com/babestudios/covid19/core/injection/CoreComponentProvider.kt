package com.babestudios.covid19.core.injection

interface CoreComponentProvider {
	fun provideCoreComponent(): CoreComponent
}
