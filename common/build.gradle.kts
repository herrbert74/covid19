import com.babestudios.covid19.buildsrc.Libs

plugins{
	id("com.babestudios.covid19.plugins.android")
}

dependencies {
	implementation(Libs.baBeStudiosBase)
	implementation(Libs.AndroidX.appcompat)
	implementation(Libs.Google.material)
}

android {
	@Suppress("UnstableApiUsage")
	buildFeatures.viewBinding = true
}
